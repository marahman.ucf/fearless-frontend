function createCard(name, description, pictureUrl, start, end, locationName) {
    return `
    <div class="col">
      <div class="card" style="box-shadow: 0px 5px 10px rgba(0, 0, 0, 0.2);">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
            <small class="text-body-secondary">${start} - ${end}</small>
        </div>
      </div>
    </div>
    `;
  }


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);
      //const errorMessage = document.getElementById('error-message');

      if (!response.ok) {
        // Figure out what to do when the response is bad
        throw new Error('Response not ok');
        //errorMessage.style.display = 'block'; // Show the error message
        //console.error("Error in response")
    } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const locationName = details.conference.location.name;

            const start = new Date(details.conference.starts);
            const formatedStart = start.toLocaleDateString();

            //const [sM,sD,sY] = [start.getMonth(), start.getDate(),start.getFullYear()] ;
            const end = new Date(details.conference.ends);
            //const [eM,eD,eY] = [end.getMonth(), end.getDate(), end.getFullYear()];
            const formatedEnd = end.toLocaleDateString();

            const html = createCard(title, description, pictureUrl, formatedStart, formatedEnd, locationName);
            const column = document.querySelector('.row');
            column.innerHTML += html;

          }
        }

      }
    } catch (e) {
      // Figure out what to do if an error is raised
      console.error("error", e)

      const errorMessage = document.getElementById('error-message');
      errorMessage.style.display = 'block'; // Show the error message
    }

  });
