import React, {useEffect, useState} from 'react';

function ConferenceForm () {
    const [conference, setConferences] = useState([]);

    const [name, setName] = useState('');

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const [startDate, setStartDate] = useState('');

    const handleStartDateChange = (event) => {
        const value = event.target.value;
        setStartDate(value);
    }

    const [endDate, setEndDate] = useState('');

    const handleEndDateChange = (event) => {
        const value = event.target.value;
        setEndDate(value);
    }

    const [description, setDescription] = useState('');

    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }

    const [maxPresentations, setMaxPresentation] = useState('');

    const handleMaxPChange = (event) => {
        const value = event.target.value;
        setMaxPresentation(value);
    }

    const [maxAttendees, setMaxAtttendee] = useState('');

    const handleMaxAChange = (event) => {
        const value = event.target.value;
        setMaxAtttendee(value);
    }
    const [location, setLocation] = useState('');

    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.name = name;
        data.starts = startDate;
        data.ends = endDate;
        data.description = description;
        data.max_presentations = maxPresentations;
        data.max_attendees = maxAttendees;
        // data.location = location;
        data.location = location;


        console.log(data);

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
          const newConference = await response.json();
          console.log(newConference);

          setName('');
          setStartDate('');
          setEndDate('');
          setDescription('');
          setMaxPresentation('');
          setMaxAtttendee('');
          setLocation('');
        }
      }





    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          console.log(data);
          setConferences(data.locations);

        }
      }

      useEffect(() => {
        fetchData();
      }, []);


    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} placeholder="Name" value={name} required type="text" name="name" id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
              </div>

              <div className="form-floating mb-3">
                <input onChange={handleStartDateChange} placeholder="Start date" value={startDate} type="date" id="starts" name="starts" required className="form-control"/>
                <label htmlFor="starts">Starts</label>
              </div>

              <div className="form-floating mb-3">
                <input onChange={handleEndDateChange} placeholder="Ends" value={endDate} type="date" id="ends" name="ends" required className="form-control"/>
                <label htmlFor="ends">Ends</label>
              </div>
              <div className="mb-3">
                <label htmlFor="description" className="form-label">Description</label>
                <textarea onChange={handleDescriptionChange} value={description} className="form-control" placeholder="Description" id="description" name="description" rows="3"></textarea>
            </div>

              <div className="form-floating mb-3">
                <input onChange={handleMaxPChange} value ={maxPresentations} placeholder="Maximum presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control"/>
                <label htmlFor="max_presentations">Maximum presentations</label>
            </div>

              <div className="form-floating mb-3">
                <input onChange={handleMaxAChange} value ={maxAttendees} placeholder="Maximum attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control"/>
                <label htmlFor="max_attendees">Maximum attendees</label>
              </div>

              <div className="mb-3">
                <select onChange={handleLocationChange} value = {location} required id="location" name="location" className="form-select">
                  <option value="">Choose a location</option>
                  {conference.map(conf => {
                        return (
                        <option key={conf.id} value={conf.id}>
                            {conf.name}
                        </option>
                        );
                    })}

                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default ConferenceForm;
